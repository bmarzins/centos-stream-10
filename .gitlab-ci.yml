---
include:
  - project: cki-project/pipeline-definition
    ref: production
    file: kernel_templates.yml

workflow: !reference [.workflow]

.10_common:
  variables:
    srpm_make_target: dist-srpm
    native_tools: 'true'

.trigger_trusted_pipeline:
  extends: [.trusted, .10_common]
  trigger:
    branch: c10s
  variables:
    kcidb_tree_name: c10s
    builder_image: quay.io/cki/builder-stream10
    kpet_tree_name: c10s

.trigger_internal_pipeline:
  extends: [.internal, .10_common]
  trigger:
    branch: rhel10
  variables:
    kcidb_tree_name: rhel-10
    builder_image: quay.io/cki/builder-stream10  # no builder-rhel10 without ubi10

.trigger_internal_compat_build_pipeline:
  extends: [.trigger_internal_pipeline, .centos_stream_rhel_internal]
  variables:
    skip_test: 'true'
    skip_results: 'true'

.trigger_scratch_pipeline:
  extends: [.trigger_internal_pipeline, .scratch]

.gcov:
  extends: [.only_build_and_publish, .coverage]

# c10s CI
c10s_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_up]

c10s_debug_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_up_debug]

c10s_rt_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_rt]

c10s_rt_debug_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_rt_debug]

c10s_rt_64k_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_rt_64k]

c10s_rt_64k_debug_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_rt_64k_debug]

c10s_automotive_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_automotive]

c10s_automotive_debug_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_automotive_debug]

c10s_64k_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_64k]

c10s_64k_debug_merge_request:
  extends: [.trigger_trusted_pipeline, .merge_request, .variant_64k_debug]

c10s_rhel10_compat_merge_request:
  extends: [.trigger_internal_compat_build_pipeline, .merge_request, .variant_up]

c10s_baseline_coverage_build:
  extends: [.trigger_trusted_pipeline, .baseline, .variant_up, .gcov]

# RHEL10 CI
rhel10_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_up]

rhel10_debug_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_up_debug]

rhel10_rt_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_rt]

rhel10_rt_debug_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_rt_debug]

rhel10_rt_64k_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_rt_64k]

rhel10_rt_64k_debug_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_rt_64k_debug]

rhel10_automotive_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_automotive]

rhel10_automotive_debug_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_automotive_debug]

rhel10_64k_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_64k]

rhel10_64k_debug_merge_request:
  extends: [.trigger_internal_pipeline, .merge_request, .variant_64k_debug]

rhel10_baseline_coverage_build:
  extends: [.trigger_internal_pipeline, .baseline, .variant_up, .gcov]

# RHEL10 private CI (including RT branches)
rhel10_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_up]

rhel10_debug_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_up_debug]

rhel10_rt_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_rt]

rhel10_rt_debug_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_rt_debug]

rhel10_rt_64k_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_rt_64k]

rhel10_rt_64k_debug_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_rt_64k_debug]

rhel10_automotive_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_automotive]

rhel10_automotive_debug_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_automotive_debug]

rhel10_64k_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_64k]

rhel10_64k_debug_merge_request_private:
  extends: [.trigger_scratch_pipeline, .merge_request, .variant_64k_debug]
